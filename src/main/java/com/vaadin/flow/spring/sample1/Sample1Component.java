package com.vaadin.flow.spring.sample1;

import com.vaadin.flow.spring.annotation.UIScope;
import org.contextualj.lang.annotation.expression.SourceClass;
import org.springframework.cop.annotation.ContextOriented;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ContextOriented
@UIScope
@SourceClass(FirstResponder.class)
@Documented
public @interface Sample1Component {
}
