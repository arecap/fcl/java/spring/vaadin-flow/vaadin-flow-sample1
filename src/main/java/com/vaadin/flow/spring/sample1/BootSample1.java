package com.vaadin.flow.spring.sample1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class BootSample1 {
}
