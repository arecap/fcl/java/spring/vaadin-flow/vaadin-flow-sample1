package com.vaadin.flow.spring.sample1.navbar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.eventbus.UiEvent;
import com.vaadin.flow.spring.eventbus.UiEventBus;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
@UiEventBus
public class NavBar extends Div implements ComponentSetup {

    @PostConstruct
    public void setup(){
        removeAll();
        setWidthFull();
        setHeight("60px");
        getStyle().set("display", "block");
    }

    @UiEvent
    public void addNavBarComponent(NavBarComponentEvent navBarComponentEvent) {
        add((Component) navBarComponentEvent.getSource());
    }

}
