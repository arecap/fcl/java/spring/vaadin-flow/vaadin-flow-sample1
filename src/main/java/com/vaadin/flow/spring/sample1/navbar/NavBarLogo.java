package com.vaadin.flow.spring.sample1.navbar;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.spring.sample1.Sample1Component;
import com.vaadin.flow.spring.sample1.Sample1Setup;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.core.annotation.Order;

@Sample1Component
@Order(0)
public class NavBarLogo extends Div implements ComponentSetup {


    @Sample1Setup
    public void setup() {
        setHeightFull();
        setWidth("300px");
        getStyle().set("float","left");
        NavBarComponentEvent.push(this);
    }

}
