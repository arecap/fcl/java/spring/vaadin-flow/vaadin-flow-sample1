package com.vaadin.flow.spring.sample1;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(BootSample1.class)
@Documented
public @interface EnableSample1 {
}
