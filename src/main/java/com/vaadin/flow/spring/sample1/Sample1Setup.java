package com.vaadin.flow.spring.sample1;

import org.contextualj.lang.annotation.expression.SourceAnnotation;
import org.contextualj.lang.annotation.pointcut.Track;

import javax.annotation.PostConstruct;
import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Track
@SourceAnnotation(PostConstruct.class)
@Documented
public @interface Sample1Setup {
}
