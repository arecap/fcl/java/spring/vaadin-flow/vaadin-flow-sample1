package com.vaadin.flow.spring.sample1.content;

import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class Content extends SplitLayout implements ComponentSetup {

    @Autowired
    private ContentMenu contentMenu;

    @Autowired
    private ContentLayout contentLayout;

    @PostConstruct
    public void setup() {
        setSizeFull();
        addToPrimary(contentMenu);
        addToSecondary(contentLayout);
    }

}
