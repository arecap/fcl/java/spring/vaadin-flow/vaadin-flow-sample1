package com.vaadin.flow.spring.sample1.content;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.eventbus.EventBus;
import org.springframework.cop.support.BeanUtil;

import java.util.EventObject;

public class ContentMenuComponentEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ContentMenuComponentEvent(Component source) {
        super(source);
    }

    public static void push(Component component) {
        EventBus eventBus = BeanUtil.getBean(EventBus.class);
        eventBus.push(new ContentMenuComponentEvent(component));
    }

}
