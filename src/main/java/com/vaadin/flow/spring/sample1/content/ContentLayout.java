package com.vaadin.flow.spring.sample1.content;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class ContentLayout extends HorizontalLayout implements ComponentSetup {

    @PostConstruct
    public void setup() {
        setSizeFull();
    }

}
