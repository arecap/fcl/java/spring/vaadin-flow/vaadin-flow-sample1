package com.vaadin.flow.spring.sample1.content;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.eventbus.UiEvent;
import com.vaadin.flow.spring.eventbus.UiEventBus;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
@UiEventBus
public class ContentMenu extends HorizontalLayout implements ComponentSetup {

    @PostConstruct
    public void setup() {
        setHeightFull();
        setWidth("400px");
        getStyle().set("max-width","50%");
    }

    @UiEvent
    public void addSideContentComponent(ContentMenuComponentEvent sideContentComponentEvent) {
        add((Component) sideContentComponentEvent.getSource());
    }

}
