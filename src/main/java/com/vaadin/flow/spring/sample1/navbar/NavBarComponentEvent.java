package com.vaadin.flow.spring.sample1.navbar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.eventbus.EventBus;
import org.springframework.cop.support.BeanUtil;

import java.util.EventObject;

public class NavBarComponentEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public NavBarComponentEvent(Component source) {
        super(source);
    }

    public static void push(Component component) {
        EventBus eventBus = BeanUtil.getBean(EventBus.class);
        eventBus.push(new NavBarComponentEvent(component));
    }

}
