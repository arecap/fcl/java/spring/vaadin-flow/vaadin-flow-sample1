package com.vaadin.flow.spring.sample1;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.spring.sample1.content.Content;
import com.vaadin.flow.spring.sample1.navbar.NavBar;
import com.vaadin.flow.spring.template.ComponentSetup;
import com.vaadin.flow.spring.template.annotation.GuiController;
import com.vaadin.flow.spring.template.annotation.ViewMapping;
import org.contextualj.lang.annotation.expression.SourceAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;

@GuiController
@SourceAnnotation(Sample1.class)
@Order(value= Ordered.HIGHEST_PRECEDENCE)
public class FirstResponder extends SplitLayout implements ComponentSetup {

    @Autowired
    private SideContent sideContent;

    @Autowired
    private NavBar navBar;

    @Autowired
    private Content content;


    @PostConstruct
    public void setup() {
        setSizeFull();
        Div primaryContent = new Div();
        primaryContent.setSizeFull();
        primaryContent.getStyle().set("overflow", "unset");
        primaryContent.add(navBar);
        Div contentWrapper = new Div();
        contentWrapper.setSizeFull();
        contentWrapper.add(content);
        primaryContent.add(contentWrapper);
        addToPrimary(primaryContent);
        addToSecondary(sideContent);
    }

    @ViewMapping
    public void templateRoute(HasComponents route) {
        Assert.isAssignable(HasSize.class, route.getClass());
        ((HasSize)route).setSizeFull();
        route.add(this);
    }

}
