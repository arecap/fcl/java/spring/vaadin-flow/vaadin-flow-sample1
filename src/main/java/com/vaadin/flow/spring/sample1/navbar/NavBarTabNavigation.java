package com.vaadin.flow.spring.sample1.navbar;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.spring.sample1.Sample1Component;
import com.vaadin.flow.spring.sample1.Sample1Setup;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.Order;

import java.util.HashMap;
import java.util.Map;

@Sample1Component
@Order(1)
public class NavBarTabNavigation extends Div implements ComponentSetup {

    private Tabs selectorTabs = new Tabs();

    private Map<Tab, String> tabsToUrl = new HashMap<>();

    @Sample1Setup
    public void setup() {
        setHeightFull();
        getStyle().set("max-width", "50%");
        getStyle().set("float","left");
        selectorTabs.setSizeFull();
        selectorTabs.addThemeVariants(TabsVariant.LUMO_SMALL);
        selectorTabs.addSelectedChangeListener(event -> {
            String url = tabsToUrl.get(selectorTabs.getSelectedTab());
            if (url != null && (UI.getCurrent().getInternals().getLastHandledLocation() == null ||
                    !UI.getCurrent().getInternals().getLastHandledLocation().getPath().contains(url))) {
                UI.getCurrent().getPage().executeJavaScript("window.location.replace(\"" + url + "\");");
            }
        });
        add(selectorTabs);
        addTabs();
        NavBarComponentEvent.push(this);
    }

    private void addTabs() {
        RouteConfiguration.forApplicationScope().getAvailableRoutes().stream().forEach(routeData -> {
            NavBarTab navBarTab = AnnotatedElementUtils.findMergedAnnotation(routeData.getNavigationTarget(), NavBarTab.class);
            if(navBarTab != null) {
                addTab(navBarTab.value(), routeData.getUrl());
            }
        });
    }

    private void addTab(String label, String url) {
        Tab tab = new Tab(label);
        selectorTabs.add(tab);
        tabsToUrl.put(tab, url);
        if(UI.getCurrent().getInternals().getLastHandledLocation().getPath().contains(url)) {
            selectorTabs.setSelectedTab(tab);
        } else {
            tab.getStyle().set("color", "#cccccc");
        }
    }

}
