package com.vaadin.flow.spring.sample1.navbar;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.sample1.Sample1Component;
import com.vaadin.flow.spring.sample1.Sample1Setup;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.cop.support.BeanUtil;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.Order;

import java.util.Arrays;

@Sample1Component
@Order(3)
public class NavBarApplicationAction extends Div implements ComponentSetup {

    @Sample1Setup
    public void setup() {
        setHeightFull();
        getStyle().set("float", "right");
        getStyle().set("display", "flex");
        getStyle().set("text-align", "right");
        getStyle().set("vertical-align", "top");
        addApplicationActions();
        NavBarComponentEvent.push(this);
    }

    private void addApplicationActions() {
        HorizontalLayout wrapper = new HorizontalLayout();
        wrapper.setPadding(false);
        wrapper.setSizeFull();
        add(wrapper);
        Arrays.stream(BeanUtil.getBeanNamesForAnnotation(ApplicationAction.class))
                .forEach(beanName -> {
                    ApplicationAction applicationAction = AnnotatedElementUtils
                            .getMergedAnnotation(BeanUtil.getBeanType(beanName), ApplicationAction.class);
                    if(applicationAction.path().isEmpty() ||
                            UI.getCurrent().getInternals().getLastHandledLocation()
                                    .getPath().contains(applicationAction.path())) {
                        wrapper.add((Component) BeanUtil.getBean(beanName));
                    }
        });
    }

}
